package com.jt.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * pojo属性自动填充配置类
 * 必须实现MetaObjectHandler接口 重写insertFill() updateFill()方法
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * setFieldValByName(arg1, arg2, arg3)
     * arg1：自动填充字段名称
     * arg2：自动填充的值
     * arg3：metaObject 固定写法
     * @param metaObject
     */
    //入库时调用
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        this.setFieldValByName("created", date, metaObject);
        this.setFieldValByName("updated", date, metaObject);
    }

    //更新时调用
    @Override
    public void updateFill(MetaObject metaObject) {
        Date date = new Date();
        this.setFieldValByName("updated", date, metaObject);

    }
}
