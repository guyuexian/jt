package com.jt.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MP配置类
 */

@Configuration //标识该类是一个配置类（代替xml文件）
public class MybatisPlusConfig {
    //铺垫：xml中通过标签管理对象，将对象交给Spring容器管理 <bean>
    //配置类：将方法的返回值交给Spring容器管理 @Bean

    /**
     * 设计一个拦截器，将分页的sql动态拼接
     * 该代码在Mybatis-plus官网粘贴
     * 修改自己的数据库源就行 DbType.MARIADB
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MARIADB));
        return interceptor;
    }
}
