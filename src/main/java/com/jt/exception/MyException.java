package com.jt.exception;

import com.jt.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理，内部采用环绕通知的方式
 * @RestControllerAdvice
 * Spring异常处理注解：捕获controller层异常，异常处理之后返回json串
 */

@RestControllerAdvice
public class MyException {

    /**
     * 业务：如果后端报错，应该及时提示用户，返回统一对象
     * SysResult对象， status=201/msg="xxx失败"
     */

    /**
     * ExceptionHandler()
     * 当遇到某种异常时，全局异常处理机制生效 - 执行该方法
     */
    @ExceptionHandler(RuntimeException.class)
    public Object exception(Exception e) {
        //打印异常信息
        e.printStackTrace();
        //返回特定的响应数据
        return SysResult.fail();
    }
}
