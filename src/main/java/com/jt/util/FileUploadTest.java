package com.jt.util;

import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@PropertySource("classpath:/image.properties")
public class FileUploadTest {

    @Value("${file.localDirPath}")
    private String localDirPath;

    @Value("${file.preURLPath}")
    private String urlPathPre;

    public ImageVO test(MultipartFile file) throws Exception{
        String fileName = file.getOriginalFilename();
        String regex = "^.+\\.(jpg|png|git)$";
        if (!fileName.matches(regex)){
            return null;
        }

        BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
        Integer height = bufferedImage.getHeight();
        Integer width = bufferedImage.getWidth();
        if (height == 0 || width == 0){
            return null;
        }

        String dateDirPath = new SimpleDateFormat("/yyyy/MM//dd/").format(new Date());
        String localDir = localDirPath + dateDirPath;

        String uuidStr = UUID.randomUUID().toString().replace("-", "");
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        String fileNamee = uuidStr+fileType;

        String virtualPath = dateDirPath + fileNamee;
        String urlPath = urlPathPre + dateDirPath + fileNamee;

        return new ImageVO(virtualPath, urlPath, fileNamee);
    }
}
