package com.jt.util;

import com.jt.vo.SysResult;

/**
 * 基础Controller类
 * 用来抽取公共的controller层代码
 */
public class ControllerUtil {
    /**
     *增删改的公共代码
     */
    public static SysResult ADU(Integer flag) {

        if (flag != 0){
            return SysResult.success();
        }
        return SysResult.fail();
    }
}
