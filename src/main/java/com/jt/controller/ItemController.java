package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.service.ItemService;
import com.jt.util.ControllerUtil;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/item")
@CrossOrigin
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/getItemList")
    public SysResult getItemList(PageResult pageResult) {
        PageResult pageResult1 = itemService.getItemList(pageResult);
        return SysResult.success(pageResult1);
    }

    @PutMapping("/updateItemStatus")
    @Transactional
    public SysResult updateItemStatus(@RequestBody Item item){
        Integer flag = itemService.updateItemStatus(item);
        return ControllerUtil.ADU(flag);
    }

    @DeleteMapping("/deleteItemById")
    @Transactional
    public SysResult deleteItemById(Integer id){
        Integer flag = itemService.deleteItemById(id);
        return ControllerUtil.ADU(flag);
    }

    /**
     * 商品新增
     * url：http://localhost:8091/item/saveItem
     * 请求类型：post
     * 请求参数：form表单提交对象 ItemVo对象接收
     * 返回值：SysResult对象
     */
    @PostMapping("/saveItem")
    @Transactional
    public SysResult saveItem(@RequestBody ItemVO itemVO){
        Integer flag = itemService.saveItem(itemVO);
        return ControllerUtil.ADU(flag);
    }
}
