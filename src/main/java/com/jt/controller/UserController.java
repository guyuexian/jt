package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getUsers")
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @PostMapping("/login")
    public SysResult login(@RequestBody User user) {
        String token = userService.login(user);
        if (StringUtils.hasLength(token)) {
            return SysResult.success(token);
        }
        return SysResult.fail();
    }

    /**
     * 分页查询
     */
    @GetMapping("/list")
    public SysResult list(PageResult pageResult) {
        PageResult pageResult1 = userService.getUserListbyMP(pageResult);
        return SysResult.success(pageResult1);
    }

    /**
     * 修改用户状态
     * @param user
     * @return
     */
    @PutMapping("/status/{id}/{status}")
    @Transactional
    public SysResult updateStatus(User user) {
        Integer count = 0;
        count = userService.updateStatus(user);
        if (count != 0) {
            return SysResult.success();
        }
        return SysResult.fail();
    }

    /**
     * 用户新增
     */
    @PostMapping("/addUser")
    @Transactional
    public SysResult addUser(@RequestBody User user) {
        Integer count = 0;
        count = userService.addUser(user);
        if (count != 0) {
            return SysResult.success();
        }
        return SysResult.fail();
    }

    /**
     * 根据id查询用户
     */
    @GetMapping("/{id}")
    public SysResult getUserById(User user) {
        User user1 = userService.getUser(user);
        if (user1 != null) {
            return SysResult.success(user1);
        }
        return SysResult.fail();
    }

    /**
     * 根据id修改用户信息
     */
    @PutMapping("/updateUser")
    @Transactional
    public SysResult updateUserById(@RequestBody User user) {
        Integer count = 0;
        count = userService.updateUserById(user);
        if (count != 0) {
            return SysResult.success();
        }
        return SysResult.fail();
    }

    /**
     * 根据id删除用户信息
     * @Transactional Spring的事务管理注解 - 采用Spring中的Aop的环绕通知，实现对事务的控制
     * 事务控制注解，默认回滚运行时异常，不回滚编译时异常
     * 事务注解自选参数
     * rollbackFor：哪种异常要回滚
     * noRollbackFor：哪种异常不回滚
     */
    @DeleteMapping("/{id}")
    @Transactional
    public SysResult deleteUserById(@PathVariable Integer id) {
        Integer count = 0;
        count = userService.deleteUserById(id);
        if (count != 0) {
            return SysResult.success();
        }
        return SysResult.fail();
    }

}
