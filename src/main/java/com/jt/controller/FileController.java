package com.jt.controller;

import com.jt.service.FileService;
import com.jt.util.ControllerUtil;
import com.jt.vo.ImageVO;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {


    @Autowired
    private FileService fileService;
    /**
     * 请求路径：http://localhost:8091/file/upload
     * 请求参数：file（binary）
     * 返回值：SysResult
     * 缓冲流 一次性搬运多大的字节效率高？？？ 1024个字节
     * MultipartFile：SpringMvc 专门针对文件上传开发的API
     */

    @PostMapping("/upload")
    public SysResult upload(MultipartFile file){
        ImageVO imageVO = fileService.upload(file);
        if (imageVO != null) {
            return SysResult.success(imageVO);
        }
        return SysResult.fail();
    }

    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath){
        boolean flag = fileService.deleteFile(virtualPath);
        if (flag){
            return SysResult.success();
        }
        return SysResult.fail();
    }

}

