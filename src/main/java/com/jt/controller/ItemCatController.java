package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/itemCat")
public class ItemCatController {

    @Autowired
    ItemCatService itemCatService;

    @GetMapping("/findItemCatList/{level}")
    public SysResult findItemCatList(@PathVariable Integer level) {
        List<ItemCat> itemCatList = itemCatService.findItemCatList2(level);
        if (itemCatList != null) {
            return SysResult.success(itemCatList);
        }
        return SysResult.fail();
    }

    @PutMapping("/status/{id}/{status}")
    @Transactional
    public SysResult updateStatus(ItemCat itemCat) {
        Integer flag = itemCatService.updateStatus(itemCat);
        return ADU(flag);
    }

    @PostMapping("/saveItemCat")
    @Transactional
    public SysResult saveItemCat(@RequestBody ItemCat itemCat) {
        if (itemCat == null){
            return SysResult.fail();
        }
        Integer flag = itemCatService.saveItemCat(itemCat);
        return ADU(flag);
    }

    /**
     * 商品分类修改
     */
    @PutMapping("/updateItemCat")
    @Transactional
    public SysResult updateItemCat(@RequestBody ItemCat itemCat) {
        Integer flag = itemCatService.updateItemCat(itemCat);
        return ADU(flag);
    }

    /**
     * 商品分类删除
     */
    @DeleteMapping("/deleteItemCat")
    @Transactional
    public SysResult deleteItemCat(ItemCat itemCat){
        Integer flag = itemCatService.deleteItemCat(itemCat);
        return ADU(flag);
    }

    /**
     *增删改的公共代码
     */
    public SysResult ADU(Integer flag) {

        if (flag != 0){
            return SysResult.success();
        }
        return SysResult.fail();
    }
}
