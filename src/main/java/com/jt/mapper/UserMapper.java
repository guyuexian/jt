package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {


    List<User> getUserList(@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize, @Param("query") String query);
}
