package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 图片上传注意事项：不要出现 - 重名文件 / 文件名特殊字符
 */

@Service
@PropertySource("classpath:/image.properties") //导入properties配置文件
public class FileServiceImpl implements FileService{

    //为属性动态赋值
    //设置本地文件夹路径
    @Value("${file.localDirPath}")
    private String localDirPath;

    //设置网络访问虚拟路径前缀
    @Value("${file.preURLPath}")
    private String preURLPath;

    /**
     * 文件上传注意事项
     * 1，验证上传的是图片 jpg|png|gif- 正则表达式
     * 2，防止木马 - 验证图片是否有高度与宽度
     * 3，文件分目录存储 - 可用时间分层 2021/06/11
     * 4，防止文件重名，修改文件名称 - UUID
     * @param file
     * @return
     */
    @Override
    public ImageVO upload(MultipartFile file) {
        //1，校验图片类型是否正确
        //1.1 获取文件名称
        String fileName = file.getOriginalFilename();
        fileName.toLowerCase(); //将名称全部小写，不转会有个bug
        //1.2 正则校验
        String regex = "^.+\\.(jpg|png|gif)$";
        if (!fileName.matches(regex)){ //若验证不通过
            return null;
        }

        //2,校验文件是否为恶意程序
        //2.1 通过图片对象获取宽度与高度
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            int height = bufferedImage.getHeight();
            int width = bufferedImage.getWidth();
            if (height == 0 || width == 0){
                //如果宽高不通过校验，终止程序
                return null;
            }

            //实现分目录存储
            //3.1 按照时间分配目录 - SimpleDateFormat类 将Date类型与特定类型的字符串进行转化
            String dateDirPath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
            String localDir = localDirPath +  dateDirPath;
            File fileDir = new File(localDir);
            if (!fileDir.exists()){
                //如果目录不存在，创建目录
                fileDir.mkdirs();
            }

            //防止文件重名，设置文件名为uuid
            String uuidStr = UUID.randomUUID().toString().replace("-", "");
            //获取最后一个 . 的坐标
            int index = fileName.lastIndexOf(".");
            //获取文件类型 - 后缀名 .jpg
            String fileType = fileName.substring(index);
            //设置完整文件路径 - 目录 + 文件名  +文件类型
            String filepath = localDir + uuidStr + fileType;
            //完成文件上传
            file.transferTo(new File(filepath));

            //创建ImageVO对象
            //设置虚拟路径 - 不要磁盘的本地存储路径
            String virtualPath = dateDirPath + uuidStr + fileType;
            //设置urlPath - 网络访问的虚拟路径 -- 网络访问前缀 + 虚拟路径
            String urlPath = preURLPath + virtualPath;
            //设置文件名 - 带后缀
            String realFileNaem = uuidStr + fileType;
            ImageVO imageVO = new ImageVO(virtualPath, urlPath, realFileNaem);
            return imageVO;
        } catch (IOException e) {
            e.printStackTrace();
        }
        //上面出现问题就会返回null
        return null;
    }

    @Override
    public boolean deleteFile(String virtualPath) {
        //获取文件全路径
        String filePath = localDirPath + virtualPath;
        //获取该文件
        File file = new File(filePath);
        //删除文件
        boolean deleteBoo = file.delete();
        return deleteBoo;
    }
}
