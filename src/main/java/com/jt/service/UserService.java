package com.jt.service;

import com.jt.pojo.User;
import com.jt.vo.PageResult;

import java.util.List;

public interface UserService {

    List<User> getUsers();

    String login(User user);

    PageResult getUserList(PageResult pageResult);

    Integer updateStatus(User user);

    Integer addUser(User user);

    User getUser(User user);

    Integer updateUserById(User user);

    Integer deleteUserById(Integer id);
    PageResult getUserListbyMP(PageResult pageResult);
}
