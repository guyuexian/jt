package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.RightsMapper;
import com.jt.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RightsServiceImpl implements RightsService {

    @Autowired
    private RightsMapper rightsMapper;


    @Override
    public List<Rights> getRightsList() {

        QueryWrapper<Rights> queryWrapper = new QueryWrapper<>(); //一级权限列表查询条件
        queryWrapper.eq("parent_id", 0);
        List<Rights> rightsList1 = rightsMapper.selectList(queryWrapper); //获取1级列表

        //查询1级下的2级list集合
        for (Rights r1:rightsList1) {
            queryWrapper.clear(); //清空已存在的queryWarp条件
            queryWrapper.eq("parent_id", r1.getId());
            List<Rights> rightsList2 = rightsMapper.selectList(queryWrapper);
            //将2级list加在1级下面
            r1.setChildren(rightsList2);
        }
        return rightsList1;
    }
}
