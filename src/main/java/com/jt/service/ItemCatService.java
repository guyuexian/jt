package com.jt.service;

import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface ItemCatService {

    List<ItemCat> findItemCatList1(Integer level);
    List<ItemCat> findItemCatList2(Integer level);

    Integer updateStatus(ItemCat itemCat);

    Integer saveItemCat(ItemCat itemCat);

    Integer updateItemCat(ItemCat itemCat);

    Integer deleteItemCat(ItemCat itemCat);
}
