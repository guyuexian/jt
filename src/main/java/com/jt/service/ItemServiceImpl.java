package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private ItemDescMapper itemDescMapper;

    @Override
    public PageResult getItemList(PageResult pageResult) {

        //获取初始页面对象
        IPage<Item> itemIPage = new Page<>(pageResult.getPageNum(), pageResult.getPageSize());
        //获取条件构造器
        QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
        //动态拼接条件
        boolean queryBoo = false;
        if (StringUtils.hasLength(pageResult.getQuery())){
            queryBoo = true;
        }
        queryWrapper.like(queryBoo, "title", pageResult.getQuery());
        //获取最终页面对象
        IPage<Item> itemIPageFinal = itemMapper.selectPage(itemIPage, queryWrapper);
        //根据最终页面对象获取分页对象的属性并封装到分页对象中
        long total = itemIPageFinal.getTotal();
        List<Item> rows = itemIPageFinal.getRecords();
        pageResult.setTotal(total);
        pageResult.setRows(rows);
        return pageResult;
    }

    @Override
    public Integer updateItemStatus(Item item) {
        return itemMapper.updateById(item);
    }

    @Override
    public Integer deleteItemById(Integer id) {

        //删除详情信息
        itemDescMapper.deleteById(id);
        //删除表信息
        return itemMapper.deleteById(id);
    }

    /**
     * 实现商品新增 Item/ItemDesc 要求ID相同
     **/
    @Override
    public Integer saveItem(ItemVO itemVO) {
        //根据ItemVo对象获取Item对象
        Item item = itemVO.getItem();
        item.setStatus(true);
        //将Item对象入库
        itemMapper.insert(item); //MP自动实现主键自动回显
        //提取回显主键
        Integer id = item.getId();

        //根据ItemVo对象获取ItemDesc对象
        ItemDesc itemDesc = itemVO.getItemDesc();
        //设置ItemDesc对象id
        itemDesc.setId(id);
        //将ItemDesc对象入库
        return itemDescMapper.insert(itemDesc);
    }
}
