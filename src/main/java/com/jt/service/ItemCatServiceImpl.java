package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Service
public class ItemCatServiceImpl implements ItemCatService {

    @Autowired
    private ItemCatMapper itemCatMapper;

    /**
     * 1，准备Map集合，实现数据封装
     * Map<key, value> = Map<parentId, List<ItemCat>>
     * 2，封装业务说明
     * map中key parentId
     *      存在：根据key获取子级list集合，将自己追加进去 形成下一个子集元素
     *      不存在：可以储存该key，同时创建一个list集合，将自己作为第一个元素封装到list中 再将list作为value存储到map中
     */
    public Map<Integer, List<ItemCat>> itemCatMap() {
        //定义Map集合
        Map<Integer, List<ItemCat>> map = new ConcurrentHashMap<>();
        //查询所有的数据库信息
        List<ItemCat> itemCatList = itemCatMapper.selectList(null);
        //遍历list，封装数据到map中
        for (ItemCat i:itemCatList){
            Integer parentId = i.getParentId();
            if (map.containsKey(parentId)) {
                //map中key，parentId存在
                //获取父集并且将自己追加到父集的子集中
                List<ItemCat> list = map.get(parentId);
                list.add(i);
            } else {
                //map中key parentId不存在，
                //将自己封装为第一个list元素存入map
                List<ItemCat> firstList = new ArrayList<>();
                firstList.add(i);
                map.put(parentId, firstList);
            }
        }
        return map;
    }

    /**
     *for循环嵌套太多层，导致程序运行速度慢，占用内存多
     */
    @Override
    public List<ItemCat> findItemCatList1(Integer level) {

        long start = System.currentTimeMillis();
        //获取一级商品分类
        QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", 0);
        List<ItemCat> itemCatList1 = itemCatMapper.selectList(queryWrapper);

        //获取二级商品分类
        for (ItemCat i1:itemCatList1) {
            queryWrapper.clear();
            queryWrapper.eq("parent_id", i1.getId());
            List<ItemCat> itemCatList2 = itemCatMapper.selectList(queryWrapper);

            //获取三级商品分类
            for (ItemCat i2:itemCatList2) {
                queryWrapper.clear();
                queryWrapper.eq("parent_id", i2.getId());
                List<ItemCat> itemCatList3 = itemCatMapper.selectList(queryWrapper);

                //将三级列表封装在二级里
                i2.setChildren(itemCatList3);
            }
            //将二级列表封装到一级里
            i1.setChildren(itemCatList2);
        }

        long end = System.currentTimeMillis();
        long time = end - start;
        System.out.println("业务执行时间为："+time+ "ms");
        return itemCatList1;
    }

    /**
     * findItemCatList1的优化
     * 要求：查询速度快
     * 思路：与数据库交互态频繁，要求值查询一次数据库就完成三级查询
     * 设计：
     *      1，查询数据库中的所有信息
     *      2，key：parentId
     *      3，value：当前parentId下的所有子级Map集合
     *      4，将查询的结果封装到 Map 集合中
     *      5，如需获取子集信息，通过getKey即可获取信息
     **/
    @Override
    public List<ItemCat> findItemCatList2(Integer level) {
        //获取封装后的map集合
        Map<Integer,List<ItemCat>> map = itemCatMap();
        //如果level=1 说明获取的是一级商品分类信息
        if (level == 1){
            return map.get(0);
        }else if (level == 2){
            return getTwoList(map);
        }

        //获取三级商品列表信息
        List<ItemCat> onelist = getTwoList(map); //一级商品列表
        for (ItemCat one:onelist){
            //获取二级商品信息
            List<ItemCat> twoList = one.getChildren();
            //如果没有二级列表 跳过本层循环
            if (twoList == null || twoList.size() == 0)
                continue;
            for (ItemCat two:twoList){
                //获取三级商品列表
                List<ItemCat> threeList = map.get(two.getId());
                //将三级商品列表封装到二级的子列表中
                two.setChildren(threeList);
            }
        }
        return onelist;
    }

    /**商品状态修改*/
    @Override
    public Integer updateStatus(ItemCat itemCat) {
        return itemCatMapper.updateById(itemCat);
    }

    /**新增商品分类*/
    @Override
    public Integer saveItemCat(ItemCat itemCat) {

        //设置商品初始状态
        itemCat.setStatus(true);
        return itemCatMapper.insert(itemCat);
    }

    @Override
    public Integer updateItemCat(ItemCat itemCat) {
        return itemCatMapper.updateById(itemCat);
    }

    /**
     * 删除改商品分类以及它的的子商品分类
     */
    @Override
    public Integer deleteItemCat(ItemCat itemCat) {

        QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
        if(itemCat.getLevel() == 3){
            //当删除的是三级商品分类，直接删除即可
            return itemCatMapper.deleteById(itemCat.getId());
        }

        if (itemCat.getLevel() == 2){
            //当删除的是二级商品分类，先删除它的子级商品分类，再删除自己
            //删除三级商品分类
            queryWrapper.eq("parent_id", itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            //删除二级商品分类
            return itemCatMapper.deleteById(itemCat.getId());
        }

        //当删除得是一级商品分类
        //获取二级商品分类,parent_id = 1级ID
        queryWrapper.eq("parent_id", itemCat.getId());
        List<ItemCat> twolist = itemCatMapper.selectList(queryWrapper);
        for (ItemCat two:twolist){
            //删除三级商品分类
            queryWrapper.clear();
            queryWrapper.eq("parent_id", two.getId());
            itemCatMapper.delete(queryWrapper);
        }
        //删除二级商品分类
        queryWrapper.clear();
        queryWrapper.eq("parent_id", itemCat.getId());
        itemCatMapper.delete(queryWrapper);
        //删除一级商品分类
        return itemCatMapper.deleteById(itemCat.getId());
    }

    /**
     * 获取两层（一级与二级）商品分类信息
     */
    public List<ItemCat> getTwoList(Map<Integer,List<ItemCat>> map) {
        //如果level=2 说明获取的是一级与二级的商品分类信息
        List<ItemCat> oneList = map.get(0);
        for (ItemCat i:oneList){
            //查询二级商品信息
            List<ItemCat> twoList = map.get(i.getId());
            //封装二级商品信息到一级中
            i.setChildren(twoList);
        }
        //二级嵌套在一级的集合中，所以永远返回的都是一级
        return oneList;
    }
}
