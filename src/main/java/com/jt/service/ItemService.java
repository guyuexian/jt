package com.jt.service;

import com.jt.pojo.Item;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;

public interface ItemService {
    PageResult getItemList(PageResult pageResult);

    Integer updateItemStatus(Item item);

    Integer deleteItemById(Integer id);

    Integer saveItem(ItemVO itemVO);
}
