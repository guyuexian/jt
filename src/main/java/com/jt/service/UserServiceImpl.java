package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getUsers() {
        return userMapper.selectList(null);
    }

    /**
     * 说明：根据用户名和密码判断数据是否有效
     * 步骤：1，将明文加密处理,md5加密/md5Hash/sha1
     *      2，根据加密后的数据查询数据库
     *      3，判断是否有效
     *          有值：返回特定字符串token=123
     *          null：u/p 不正确 返回null
     * @param user
     * @return
     */
    @Override
    public String login(User user) {
        //获取原始密码
        String password = user.getPassword();
        //将密码进行加密处理
        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
        //将加密后的密码封装到user对象中
        user.setPassword(md5Str);
        //查询数据库
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
        User userDB = userMapper.selectOne(queryWrapper);
        //定义token数据 限定条件 token不能重复 UUID
        String uuidStr = UUID.randomUUID().toString().replace("-", "");
        //三目运算判断并返回
        return userDB==null?null:uuidStr;
    }

    @Override
    public PageResult getUserList(PageResult pageResult) {

        //创建条件构造器
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //获取每页记录数
        Integer pageSize = pageResult.getPageSize();
        //获取分页起始位置
        Integer startIndex = (pageResult.getPageNum() - 1)*pageSize;
        //获取查询条件
        String query = pageResult.getQuery();
        //添加动态查询条件
        boolean queryBoo = StringUtils.hasLength(query);
        queryWrapper.like(queryBoo, "username", query);
        //查询数据总记录数
        long total = userMapper.selectCount(queryWrapper);
        //查询分页结果
        List<User> rows = userMapper.getUserList(startIndex, pageSize, query);
        //将查询的数据封装到pageResult对象中
        pageResult.setTotal(total);
        pageResult.setRows(rows);
        return pageResult;
    }

    /**
     * 利用MP的形式实现分页查询
     * userMapper.selectPage(arg1, arg2)
     * arg1：MP的分页对象page
     * arg2：条件构造器queryWrapper
     *
     * 创建分页对象new Page(arg1, arg2)
     * arg1：页码
     * arg2：每页记录数
     */
    public PageResult getUserListbyMP(PageResult pageResult) {
        //创建MP的分页对象
        IPage page = new Page(pageResult.getPageNum(), pageResult.getPageSize());
        //创建条件构造器
        QueryWrapper queryWrapper = new QueryWrapper();
        //动态添加条件
        Boolean queryBoo = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(queryBoo, "username", pageResult.getQuery());
        //获取结果分页对象
        IPage page1 = userMapper.selectPage(page, queryWrapper);
        //获取总记录数
        long total = page1.getTotal();
        //获取当前分页结果
        List<User> rows = page1.getRecords();
        //封装总记录数与页面数据到pageResult中
        pageResult.setTotal(total);
        pageResult.setRows(rows);
        //返回PageResult对象
        return pageResult;
    }

    @Override
    public Integer updateStatus(User user) {
        //写入数据库
        int i = userMapper.updateById(user);
        return i;

    }

    @Override
    public Integer addUser(User user) {
        //加密用户密码
        String password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        //将密码封装到用户信息中
        user.setPassword(password);
        //设置用户初始状态
        user.setStatus(true);
        //实现入库操作
        return userMapper.insert(user);
    }

    @Override
    public User getUser(User user) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public Integer updateUserById(User user) {
        //加密密码并封装如数据库
        String password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(password);
        return userMapper.updateById(user);
    }

    @Override
    public Integer deleteUserById(Integer id) {
        return userMapper.deleteById(id);
    }
}
