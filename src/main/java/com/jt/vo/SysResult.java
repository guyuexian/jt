package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysResult {
    private Integer status; //状态码的信息
    private String msg; //提示信息
    private Object data; //封装业务的数据

    //封装成功与失败方法
    public static SysResult fail() {
        return new SysResult(201, "服务器调用失败", null);
    }

    public static SysResult success() {
        return new SysResult(200, "业务调用成功", null);
    }

    public static SysResult success(Object object) {
        return new SysResult(200, "业务调用成功", object);
    }

    public static SysResult success(String msg, Object object) {
        return new SysResult(200, msg, object);
    }
}
