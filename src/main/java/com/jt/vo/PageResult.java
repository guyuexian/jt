package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class PageResult {
    private String query; //分页查询条件 可以为空
    private Integer pageNum; //查询页数
    private Integer pageSize; //查询条数
    private Long total; //查询总记录数
    private Object rows; //分页查询的结果
}
